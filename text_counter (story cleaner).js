let story = 'Last weekend, I took literally the most beautiful bike ride of my life. The route is called "The 9W to Nyack" and it actually stretches all the way from Riverside Park in Manhattan to South Nyack, New Jersey. It\'s really an adventure from beginning to end! It is a 48 mile loop and it basically took me an entire day. I stopped at Riverbank State Park to take some extremely artsy photos. It was a short stop, though, because I had a really long way left to go. After a quick photo op at the very popular Little Red Lighthouse, I began my trek across the George Washington Bridge into New Jersey.  The GW is actually very long - 4,760 feet! I was already very tired by the time I got to the other side.  An hour later, I reached Greenbrook Nature Sanctuary, an extremely beautiful park along the coast of the Hudson.  Something that was very surprising to me was that near the end of the route you actually cross back into New York! At this point, you are very close to the end.';

let overusedWords = ['really', 'very', 'basically'];
let unnecessaryWords = ['extremely', 'literally', 'actually' ];

// save words as elements in new array
let storyWords = story.split(' ');
console.log(`There are ${storyWords.length} words in the story.`); // count words
console.log();

// filter out unnecessary words in new array
let betterWords = storyWords.filter(word => !unnecessaryWords.includes(word));

// Function: counts how many times element appears in array
const countWords = (array, word) => {
  let onlyWords = array.filter(element => element === word);
  let length = onlyWords.length;
  console.log(`Word "${word}" appears in text ${length} times.`)
  return length;
}

const countWords2 = (array, word) => {
  let counter = 0;
  for (let i = 0; i < array.length; i++) {
    if (word === array[i]) {counter++;}
  }
  console.log(`Word "${word}" appears in text ${counter} times.`)
  return counter;
}
//

// count overused words
overusedWords.forEach( word => countWords(betterWords, word));
console.log()

// Function: counts sentences in array
const countSentences = string => {
  sentencesArray = string.split(/\.|!/);
  sentences = sentencesArray.length -1;
  console.log(`There are ${sentences} sentences in the story.`)
  return sentences;
}

const countSentences2 = array => {
  let counter = 0;
  for (let i=0; i < array.length; i++) {
    if (array[i][array[i].length - 1] === "." || array[i][array[i].length - 1] === "!" || array[i][array[i].length - 1] === "?") {counter++;}
  }
  console.log(`There are ${counter} sentences in the story.`)
  return counter;
}

const countSentences3 = array => {
  let sentences = 0;
array.forEach(word => {
  if (word[word.length-1] === '.' || word[word.length-1] === '!') {
    sentences+=1;
  }
});
  console.log(`There are ${sentences} sentences in the story.`)
  return sentences;
}
//

// count sentences
countSentences(story);
console.log();

// print improved story
console.log("====== IMROVED VERSION ======")
console.log(betterWords.join(' '));
console.log();
console.log(`There are ${betterWords.length} words in the improved version of story.`); // count words

/* 
Here are some ideas:

- For the overused words, remove it every other time it appears.
- Write a function that finds the word that appears the greatest number of times.
- Replaced overused words with something else.

*/
