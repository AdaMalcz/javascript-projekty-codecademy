const menu = {
  _courses: {
    appetizers: [],
    mains: [],
    desserts: [], 
  },
    
  get appetizers() {
      return this._courses.appetizers;
    },
  set appetizers(appetizers) {
      this._courses.appetizers = appetizers;
    },
    
  get mains() {
      return this._courses.mains;
    },
   set mains(mains) {
      this._courses.mains = mains;
    },
    
  get desserts() {
      return this._courses.desserts;
    },
  set desserts(desserts) {
      this._courses.desserts = desserts;
    },
  
  get courses() {
      return {
        appetizers: this.appertizers,
        mains: this.mains,
        desserts: this.desserts
      }
    },
  
  addDishToCourse(courseName, dishName, dishPrice) {
      const dish = {
        name: dishName,
        price: dishPrice
      };
      this._courses[courseName].push(dish);
    },
  
  getRandomDishFromCourse(courseName) {
      const dishes =  this._courses[courseName];
      return dishes[Math.floor(Math.random() * dishes.length)];
    },
  
  generateRandomMeal() {
      const appetizer = this.getRandomDishFromCourse('appetizers');
      const main = this.getRandomDishFromCourse('mains');
      const dessert = this.getRandomDishFromCourse('desserts');
      const total = appetizer.price + main.price + dessert.price;
      
      return `
Appetizer: ${appetizer.name}
Main Dish: ${main.name}
Dessert: ${dessert.name}

Total price: ${Math.round(total * 100) / 100}`;
    }
};

menu.addDishToCourse('appetizers', 'Caesar Salad', 4.25);
menu.addDishToCourse('appetizers', 'Pharaoph Salad', 4.45);
menu.addDishToCourse('appetizers', 'Farmer Salad', 3.25);
menu.addDishToCourse('mains', 'Pizza', 12.95);
menu.addDishToCourse('mains', 'Soup', 8.95);
menu.addDishToCourse('mains', 'Rice', 6.95);
menu.addDishToCourse('desserts', 'Cookie', 1.99);
menu.addDishToCourse('desserts', 'Cake', 5.99);
menu.addDishToCourse('desserts', 'Cocaine', 80.00);

let meal = menu.generateRandomMeal();
console.log(meal);