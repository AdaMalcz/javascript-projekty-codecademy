const getUserChoice = userInput => {
  userInput = userInput.toLowerCase();
  if (userInput === 'rock' || userInput === 'paper' || userInput === 'scissors') {
    return userInput;
  } else {
    console.log('Wrong item!');
  }
};

const getComputerChoice = () => {
  let choice = Math.floor(Math.random()*3);
  switch (choice) {
    case 0:
      return 'paper';
    case 1:
      return 'rock';
    case 2:
      return 'scissors';
  }
};

const determineWinner = (userChoice, computerChoice) => {
  if (userChoice === computerChoice) {
    return 'There was a tie.';
  } else {
    if (userChoice === 'rock') {
      if (computerChoice === 'paper') {
        return 'You lost!';
      } else {
        return 'You won!';
      }
    } else if (userChoice === 'paper') {
      if (computerChoice === 'scissors') {
        return 'You lost!';
      } else {
        return 'You won!';
      }
    } else {
      if (computerChoice === 'rock') {
        return 'You lost!';
      } else {
        return 'You win!';
      }
    }
  }
};

function playGame(item) {
  const userChoice = getUserChoice(item);
  const computerChoice = getComputerChoice();
  console.log(`Computer: ${computerChoice}`);
  console.log(`You: ${userChoice}`);
  console.log();
  console.log(determineWinner(userChoice, computerChoice));
}

playGame('paper');
