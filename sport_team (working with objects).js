const team = {
  _players: [
    {
  firstName: 'Pablo',
  lastName: 'Sanchez',
  age: 11
},
    {
  firstName: 'Tomcio',
  lastName: 'Zjeb',
  age: 12
},
    {
  firstName: 'Rafael',
  lastName: 'Hooy',
  age: 14
}
  ],
  _games: [
    {
  opponent: 'Broncos',
  teamPoints: 42,
  opponentPoints: 27
},
    {
  opponent: 'Hujowaz',
  teamPoints: 69,
  opponentPoints: 78
},
    {
  opponent: 'Nieboraki',
  teamPoints: 99,
  opponentPoints: 1
}
  ],
  
  get players() {return this._players;},
  get games() {return this._games;},
  
  addPlayer(firstName, lastName, age) {
    let player = {
      firstName: firstName,
      lastName: lastName,
      age: age
    };
    this._players.push(player);
  },
  
  addGame(oponnent, teamPoints, opponentPoints) {
    let game = {
      oponnent: oponnent,
      teamPoints: teamPoints,
      opponentPoints: opponentPoints
    };
    this._games.push(game);
  }
};

team.addPlayer('Steph', 'Curry', 28);
team.addPlayer('Lisa', 'Lesile', 44);
team.addPlayer('Bugs', 'Bunny', 76);

console.log(team.players);
team.addGame('Titans', 100, 98);
console.log(team.games);
